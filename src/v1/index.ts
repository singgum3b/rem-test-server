import {Controller, Ctx, Get, Param, Post, UploadedFile, UseAfter} from "routing-controllers";
import {fileAsString} from "../utils/file-as-string";
import * as multer from "multer";
import {Context} from "koa";
import * as koaStatic from "koa-static";

const index = fileAsString("public/index.html", process.cwd());

const uploadOptions = {
    storage: multer.diskStorage({
        destination: function (_, __, cb) {
            cb(null, './public/uploaded/')
        },
        filename: function (_, file, cb) {
            console.log(file.filename, file.originalname);
            cb(null, Date.now() + "-" + file.originalname)
        }
    }),
    limits: {
        fieldNameSize: 255,
        fileSize: 1024 * 1024 // 1MB
    }
};

@Controller()
export class IndexController {

    @Get("/")
    public async index() {
        return index;
    }

    @Get("/uploaded/:filename")
    @UseAfter(koaStatic(process.cwd() + "/public/uploaded", { maxage: 500000 }))
    public async getFile(@Ctx() ctx: Context, @Param("filename") filename: string): Promise<any> {
        ctx.path = `/${filename}`;
        return ctx;
    }

    @Post("/upload")
    public async upload(@UploadedFile("upload", { options: uploadOptions}) file: Express.Multer.File, @Ctx() _: Context) {
        const {buffer, path, destination, ...rest} = file;
        return rest;
    }

}
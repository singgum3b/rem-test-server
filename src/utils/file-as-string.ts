import * as fs from "fs";

export async function fileAsString(p: string, dir: string): Promise<string> {

    const filename = require.resolve(p, { paths: [dir]});
    return new Promise<string>((resolve, reject): void => {
        try {
            fs.readFile(filename, "utf8", (err, data) => {
                if (err) { reject(err) }
                resolve(data);
            });
        } catch (e) {
            reject(e);
        }
    })

}

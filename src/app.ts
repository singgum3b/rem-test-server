import {Container} from "typedi";
import {createKoaServer, useContainer as rcUseContainer} from "routing-controllers";
import * as Application from "koa";

rcUseContainer(Container);

const PORT = process.env.API_PORT || 8080;

export class App {

    private app: Application;

    constructor() {
        this.app = createKoaServer({
            controllers: [__dirname + "/v1/*.ts"],
        });
    }

    public async start(): Promise<void> {
        this.app.listen(PORT, () => {
            console.log(`App listening on port ${PORT}`);
        });
    }

}
